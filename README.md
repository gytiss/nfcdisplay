# Batteryless NFC Powered Display

This device consists of an NFC powered processor board and a 2.5cm x 2.5cm E-Paper display. It does not require __any__ battery power. To change the image on the display all one needs is an NFC enabled smartphone. 

Both the processor board hardware and software were designed and built by myself.

__Note:__ This is a prototype device, hence neither hardware nor software are of production quality.


# Parts

It uses a low power STM32 MCU, ST NFC chip with energy harvesting output, a low voltage DC/DC converter, a couple of super capacitors, diodes and some passives.

# Operation in a Nutshell

Super capacitors are charged by the energy hardvested from the NFC field of a smartphone. When the voltage is high enough MCU wakes up in low power mode and starts communicating with the smatphone via NFC in order to receive image to be uploaded to the screen and to send status updates. Once the super capacitors have enough charge MCU triggers E-Paper screen update.

# Images:
![Assembled Board](Images/AssembledBoard.jpg)

![Board Top](Images/BoardTop.png)

![Board Bottom](Images/BoardBottom.png)

![3D Rendering](Images/3DRendering.png)

![Finished Device](Images/FullDevice.jpg)