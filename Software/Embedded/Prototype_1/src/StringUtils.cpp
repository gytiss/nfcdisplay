#include "StringUtils.hpp"

namespace StringUtils
{
	bool ByteArrayToHexString(const uint8_t * byteArray, const uint32_t numberOfBytesInTheArray, char* outString, const uint32_t maxOutStringLength, uint32_t* outStrLen)
	{
		bool retVal = false;

		*outStrLen = 0;
		const char* outStringStartPointer = outString;

		if((byteArray != nullptr) && (numberOfBytesInTheArray > 0) && (outString != nullptr) && (maxOutStringLength > 0))
		{
			const uint32_t hexStrLengthPerByte = 4u;
			const uint32_t totalStrLengthRequired = ((numberOfBytesInTheArray * hexStrLengthPerByte) + (numberOfBytesInTheArray - 1) + 1);

			if (totalStrLengthRequired <= maxOutStringLength)
			{
				retVal = true;
				for (uint32_t i = 0; i < (numberOfBytesInTheArray - 1); i++)
				{
					ByteToHexStr(byteArray[i], outString);
					outString += hexStrLengthPerByte;
					*outString = ' ';
					outString++;
				}

				ByteToHexStr(byteArray[(numberOfBytesInTheArray - 1)], outString);
				outString += hexStrLengthPerByte;
				*outString = '\0';

				*outStrLen = (outString - outStringStartPointer);
			}
		}

		return retVal;
	}
}
