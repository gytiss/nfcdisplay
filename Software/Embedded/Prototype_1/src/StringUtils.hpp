
#ifndef STRINGUTILS_HPP_INCLUDED
#define STRINGUTILS_HPP_INCLUDED

#include <cstdint>

namespace StringUtils
{
	inline void ByteToHexStr(uint8_t byte, char outHexStr[5])
	{
		const uint8_t lowNibble = (byte & 0x0F);
		const uint8_t highNibble = ((byte >> 4) & 0x0F);
		outHexStr[0] = '0';
		outHexStr[1] = 'x';
		outHexStr[2] = ((highNibble <= 9) ? ('0' + highNibble) : ('A' + (highNibble - 10)));
		outHexStr[3] = ((lowNibble <= 9) ? ('0' + lowNibble) : ('A' + (lowNibble - 10)));
		outHexStr[4] = '\0';
	}

	bool ByteArrayToHexString(const uint8_t * byteArray, const uint32_t numberOfBytesInTheArray, char* outString, const uint32_t maxOutStringLength, uint32_t* outStrLen);
}

#endif // STRINGUTILS_HPP_INCLUDED
