
#ifndef EPAPER_SCREEN_H_INCLUDED 
#define EPAPER_SCREEN_H_INCLUDED

#include <stdint.h>

#ifdef __cplusplus
extern "C"
{
#endif

void EpaperScreen_Init();

void EpaperScreen_Display(uint8_t imageBuffer[5000]);

#ifdef __cplusplus
}
#endif

#endif // EPAPER_SCREEN_H_INCLUDED
