
#ifndef MILLISECONDTIMER_HPP_INCLUDE
#define MILLISECONDTIMER_HPP_INCLUDE

#include <cstdint>

struct __TIM_HandleTypeDef;

typedef __TIM_HandleTypeDef TIM_HandleTypeDef;

class MillisecondTimer
{
public:
	MillisecondTimer(TIM_HandleTypeDef& timerHandle);

	/**
     * Sleep for a given number of milliseconds
     *
     * @param milliseconds to sleep for
	 */
	void sleepForMs(uint32_t milliseconds);

private:
	enum class State
	{
		Idle,
		TimerInterruptReceived
	};

	static void _timerInterruptHandler(void* thisPtr);

	TIM_HandleTypeDef& m_TimerHandle;
	State m_State;
	uint32_t m_ConfiguredSleepTime;
};

#endif // MILLISECONDTIMER_HPP_INCLUDE
