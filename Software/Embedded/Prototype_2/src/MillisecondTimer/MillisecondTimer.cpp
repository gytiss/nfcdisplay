#include "MillisecondTimer.hpp"

#include <stm32l4xx_hal.h>
#include "stm32l4xx_it.h"

volatile uint32_t timerIntCounter__ = 0;

MillisecondTimer::MillisecondTimer(TIM_HandleTypeDef& timerHandle)
: m_TimerHandle(timerHandle),
  m_State(State::Idle),
  m_ConfiguredSleepTime(0)
{
	Register_InterruptsHandler(InterruptID_TIM2, _timerInterruptHandler, this);

	uint32_t prescaler = 0;
	if (prescaler > 0)
	{
		prescaler--;
	}
	m_TimerHandle.Init.Prescaler = prescaler; // Every one millisecond
	m_TimerHandle.Init.Period = 0;
	if (HAL_TIM_Base_Init(&m_TimerHandle) != HAL_OK)
	{
		_Error_Handler(__FILE__, __LINE__);
	}
}
volatile uint32_t ttttttttttttttttttttt = 0;
volatile uint32_t ttttttttttttttttttttt___ = 0;
void MillisecondTimer::sleepForMs(uint32_t milliseconds)
{
	ttttttttttttttttttttt = 0;
	ttttttttttttttttttttt___ = 0;

	HAL_TIM_Base_Stop_IT(&m_TimerHandle);

	m_State = State::Idle;

	if (milliseconds > 0)
	{
		if (milliseconds != m_ConfiguredSleepTime)
		{
			m_ConfiguredSleepTime = milliseconds;

			const uint32_t ticksPerMillisecond = (HAL_RCC_GetHCLKFreq() / 1000);

			m_TimerHandle.Init.Period = ((milliseconds * ticksPerMillisecond) - 1);
			if (HAL_TIM_Base_Init(&m_TimerHandle) != HAL_OK)
			{
				_Error_Handler(__FILE__, __LINE__);
			}

			__HAL_TIM_CLEAR_FLAG(&m_TimerHandle, TIM_IT_UPDATE);
		}

		//__HAL_TIM_SET_COUNTER(&m_TimerHandle, 0);

		//__HAL_TIM_SET_COUNTER(&m_TimerHandle, 0);
		//__HAL_TIM_CLEAR_FLAG(&m_TimerHandle, TIM_IT_UPDATE);
		//__HAL_TIM_ENABLE_IT(&m_TimerHandle, TIM_IT_UPDATE);

		HAL_SuspendTick();
		HAL_TIM_Base_Start_IT(&m_TimerHandle);

//		volatile uint32_t counterValue = __HAL_TIM_GET_COUNTER(&m_TimerHandle);
//		while(counterValue > 0)
//		{
//			// Enter sleep mode
//			HAL_PWR_EnterSLEEPMode(PWR_MAINREGULATOR_ON, PWR_SLEEPENTRY_WFI);
//
//			counterValue = __HAL_TIM_GET_COUNTER(&m_TimerHandle);
//		}

		while (m_State != State::TimerInterruptReceived)
		{
			__HAL_FLASH_SLEEP_POWERDOWN_ENABLE();
			HAL_PWR_EnterSLEEPMode(PWR_LOWPOWERREGULATOR_ON, PWR_SLEEPENTRY_WFI);
			ttttttttttttttttttttt++;
		}
		__HAL_FLASH_SLEEP_POWERDOWN_DISABLE();

		HAL_TIM_Base_Stop_IT(&m_TimerHandle);
		HAL_ResumeTick();
	}
}

void MillisecondTimer::_timerInterruptHandler(void* thisPtr)
{
	volatile MillisecondTimer* timerPtr = static_cast<MillisecondTimer*>(thisPtr);
	timerPtr->m_State = State::TimerInterruptReceived;
	timerIntCounter__++;
}

