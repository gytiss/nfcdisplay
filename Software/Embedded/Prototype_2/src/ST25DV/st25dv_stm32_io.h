
#ifndef ST25DV_ST25DV_STM32_IO_H_INCLUDED
#define ST25DV_ST25DV_STM32_IO_H_INCLUDED

#include "stm32l4xx_hal.h"

#ifdef __cplusplus
extern "C"
{
#endif

void ST25DV_STM32_IO_Init(I2C_HandleTypeDef* i2cDevice);

#ifdef __cplusplus
}
#endif

#endif // ST25DV_ST25DV_STM32_IO_H_INCLUDED
