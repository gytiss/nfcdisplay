
#include "st25dv_stm32_io.h"
#include "st25dv.h"


static I2C_HandleTypeDef* g_I2cDevice = NULL;

/**
 * @brief  NFC04A1 Ack Nack enumerator definition
 */
typedef enum
{
  I2CANSW_ACK = 0,
  I2CANSW_NACK
} NFC04A1_I2CANSW_E;

void ST25DV_STM32_IO_Init(I2C_HandleTypeDef* i2cDevice)
{
	g_I2cDevice = i2cDevice;
}

/**
  * @brief  Write data in a register of the device through the bus
  * @param  pData : pointer to the data to write
  * @param  DevAddr : Target device address
  * @param  TarAddr : I2C data memory address to write
  * @param  Size : Size in bytes of the value to be written
  * @retval HAL status
  */

static HAL_StatusTypeDef STM32_I2C1_MemWrite( const uint8_t * const pData, const uint8_t DevAddr, const uint16_t TarAddr, const uint16_t Size )
{
  uint8_t *pbuffer = (uint8_t *)pData;

  return HAL_I2C_Mem_Write( g_I2cDevice, DevAddr, TarAddr, I2C_MEMADD_SIZE_16BIT, pbuffer, Size, ST25DV_I2C_TIMEOUT );
}

/**
  * @brief  Read the value of a register of the device through the bus
  * @param  pData : pointer to store read data
  * @param  DevAddr : Target device address
  * @param  TarAddr : I2C data memory address to read
  * @param  Size : Size in bytes of the value to be read
  * @retval HAL status.
  */
static HAL_StatusTypeDef STM32_I2C1_MemRead( uint8_t * const pData, const uint8_t DevAddr, const uint16_t TarAddr, const uint16_t Size )
{
  uint8_t *pbuffer = (uint8_t *)pData;
  HAL_StatusTypeDef ret;

  ret = HAL_I2C_Mem_Read( g_I2cDevice, DevAddr, TarAddr, I2C_MEMADD_SIZE_16BIT, pbuffer, Size, ST25DV_I2C_TIMEOUT );

  return ret;
}

/**
  * @brief  Read the value of a register of the device through the bus
  * @param  pData : pointer to store read data
  * @param  DevAddr : the device address on bus
  * @param  Size : Size in bytes of the value to be read
  * @retval HAL status
  */
static HAL_StatusTypeDef STM32_I2C1_Read( uint8_t * const pData, const uint8_t DevAddr, const uint16_t Size )
{
  uint8_t *pbuffer = (uint8_t *)pData;
  HAL_StatusTypeDef ret;

  ret = HAL_I2C_Master_Receive( g_I2cDevice, DevAddr, pbuffer, Size, ST25DV_I2C_TIMEOUT );

  return ret;
}

/**
* @brief  Checks if NACK was received from I2C Slave
* @param  None
* @retval 0 ACK, 1 NACK
*/
static uint8_t STM32_I2C1_IsNacked( void )
{
  if( g_I2cDevice->ErrorCode == HAL_I2C_ERROR_AF )
  {
    return I2CANSW_NACK;
  }
  return I2CANSW_ACK;
}

/**
* @brief  Checks if target device is ready for communication
* @param  DevAddr : Target device address
* @param  Trials : Number of trials
* @retval HAL status
*/
static HAL_StatusTypeDef STM32_I2C1_IsDeviceReady( const uint8_t DevAddr, const uint32_t Trials )
{
  return HAL_I2C_IsDeviceReady( g_I2cDevice, DevAddr, Trials, ST25DV_I2C_TIMEOUT );
}












/******************************** LINK NFCTAG *****************************/
/**
  * @brief  This functions converts HAL status to NFCTAG status
  * @param  status : HAL status to convert
  * @retval NFCTAG enum status
  */
NFCTAG_StatusTypeDef NFCTAG_ConvertStatus( const HAL_StatusTypeDef status )
{
  switch( status )
  {
    case HAL_OK:
      return NFCTAG_OK;
    case HAL_ERROR:
      return NFCTAG_ERROR;
    case HAL_BUSY:
      return NFCTAG_BUSY;
    case HAL_TIMEOUT:
      return NFCTAG_TIMEOUT;

    default:
      return NFCTAG_TIMEOUT;
  }
}

/**
  * @brief  Configures nfctag I2C interface
  * @param  None
  * @retval NFCTAG enum status
  */
static NFCTAG_StatusTypeDef NFCTAG_IO_Init( void )
{
//  NFC04A1_GPO_Init( );
//
//  NFC04A1_LPD_Init( );
//
//  return NFCTAG_ConvertStatus( STM32_I2C1_Init( ST25DV_I2C_SPEED ) );

  return NFCTAG_OK;
}

/**
  * @brief  Write at specific address nfctag memory
  * @param  pData : pointer to the data to write
  * @param  DevAddr : Target device address
  * @param  TarAddr : I2C data memory address to write
  * @param  Size : Size in bytes of the value to be written
  * @retval NFCTAG enum status
  */
NFCTAG_StatusTypeDef NFCTAG_IO_MemWrite( const uint8_t * const pData, const uint8_t DevAddr, const uint16_t TarAddr, const uint16_t Size )
{
  return NFCTAG_ConvertStatus( STM32_I2C1_MemWrite( pData, DevAddr, TarAddr, Size ) );
}

/**
  * @brief  Read at specific address on nfctag
  * @param  pData : pointer to store read data
  * @param  DevAddr : Target device address
  * @param  TarAddr : I2C data memory address to read
  * @param  Size : Size in bytes of the value to be read
  * @retval NFCTAG enum status
  */
NFCTAG_StatusTypeDef NFCTAG_IO_MemRead( uint8_t * const pData, const uint8_t DevAddr, const uint16_t TarAddr, const uint16_t Size )
{
  return NFCTAG_ConvertStatus( STM32_I2C1_MemRead( pData, DevAddr, TarAddr, Size ) );
}

/**
  * @brief  Read at current address on nfctag
  * @param  pData : pointer to store read data
  * @param  DevAddr : Target device address
  * @param  Size : Size in bytes of the value to be read
  * @retval NFCTAG enum status
  */
NFCTAG_StatusTypeDef NFCTAG_IO_Read( uint8_t * const pData, const uint8_t DevAddr, const uint16_t Size )
{
  return NFCTAG_ConvertStatus( STM32_I2C1_Read( pData, DevAddr, Size ) );
}

/**
  * @brief  Checks if NACK was received from I2C Slave
  * @param  None
  * @retval 0 ACK, 1 NACK
  */
uint8_t NFCTAG_IO_IsNacked( void )
{
  return STM32_I2C1_IsNacked( );
}

/**
  * @brief  Check nfctag availability
  * @param  DevAddr : Target device address
  * @param  Trials : Number of trials
  * @retval NFCTAG enum status
  */
NFCTAG_StatusTypeDef NFCTAG_IO_IsDeviceReady( const uint8_t DevAddr, const uint32_t Trials )
{
  return NFCTAG_ConvertStatus( STM32_I2C1_IsDeviceReady( DevAddr, Trials ) );
}



/******************************** LINK EEPROM COMPONENT *****************************/

/**
  * @brief  Initializes peripherals used by the I2C NFCTAG driver
  * @param  None
  * @retval NFCTAG enum status
  */
NFCTAG_StatusTypeDef ST25DV_IO_Init( void )
{
  return NFCTAG_IO_Init( );
}

/**
  * @brief  Write data, at specific address, through i2c to the ST25DV
  * @param  pData: pointer to the data to write
  * @param  DevAddr : Target device address
  * @param  TarAddr : I2C data memory address to write
  * @param  Size : Size in bytes of the value to be written
  * @retval NFCTAG enum status
  */
NFCTAG_StatusTypeDef ST25DV_IO_MemWrite( const uint8_t * const pData, const uint8_t DevAddr, const uint16_t TarAddr, const uint16_t Size )
{
  NFCTAG_StatusTypeDef pollstatus;
  NFCTAG_StatusTypeDef ret;
  uint32_t tickstart;

  ret = NFCTAG_IO_MemWrite( pData, DevAddr, TarAddr, Size );
  if( ret == NFCTAG_OK )
  {
    /* Poll until EEPROM is available */
    tickstart = HAL_GetTick();
    /* Wait until ST25DV is ready or timeout occurs */
    do
    {
      pollstatus = ST25DV_IO_IsDeviceReady( DevAddr, 1 );
    } while( ( (HAL_GetTick() - tickstart) < ST25DV_I2C_TIMEOUT) && (pollstatus != NFCTAG_OK) );

    if( pollstatus != NFCTAG_OK )
    {
      ret = NFCTAG_TIMEOUT;
    }
  }
  else
  {
    /* Check if Write was NACK */
    if( ST25DV_IO_IsNacked() == I2CANSW_NACK )
    {
      ret = NFCTAG_NACK;
    }
  }

  return ret;
}

/**
  * @brief  Reads data at a specific address from the NFCTAG.
  * @param  pData: pointer to store read data
  * @param  DevAddr : Target device address
  * @param  TarAddr : I2C data memory address to read
  * @param  Size : Size in bytes of the value to be read
  * @retval NFCTAG enum status
  */
NFCTAG_StatusTypeDef ST25DV_IO_MemRead( uint8_t * const pData, const uint8_t DevAddr, const uint16_t TarAddr, const uint16_t Size )
{
  return NFCTAG_IO_MemRead( pData, DevAddr, TarAddr, Size );
}

/**
  * @brief  Reads data at current address from the NFCTAG.
  * @param  pData: pointer to store read data
  * @param  DevAddr : Target device address
  * @param  Size : Size in bytes of the value to be read
  * @retval NFCTAG enum status
  */
NFCTAG_StatusTypeDef ST25DV_IO_Read( uint8_t * const pData, const uint8_t DevAddr, const uint16_t Size )
{
  return NFCTAG_IO_Read( pData, DevAddr, Size );
}

/**
  * @brief  Checks if NACK was received from I2C Slave
  * @param  None
  * @retval 0 ACK, 1 NACK
  */
uint8_t ST25DV_IO_IsNacked( void )
{
  return NFCTAG_IO_IsNacked( );
}

/**
* @brief  Checks if target device is ready for communication
* @note   This function is used with Memory devices
* @param  DevAddr : Target device address
* @param  Trials : Number of trials
* @retval NFCTAG enum status
*/
NFCTAG_StatusTypeDef ST25DV_IO_IsDeviceReady( const uint8_t DevAddr, const uint32_t Trials )
{
  return NFCTAG_IO_IsDeviceReady( DevAddr, Trials );
}








