
#include "EpaperScreen.h" 
#include "e-Paper/EPD_1in54.h"
#include "e-Paper/DEV_Config.h"
#include "e-Paper/GUI_Paint.h"
#include "e-Paper/ImageData.h"

//Create a new image cache
//static UBYTE BlackImage[((EPD_WIDTH % 8 == 0)? (EPD_WIDTH / 8 ): (EPD_WIDTH / 8 + 1)) * EPD_HEIGHT];

void EpaperScreen_Init()
{
	DEV_Digital_Write(EPD_CS_PIN, 1);

	if (EPD_Init(lut_full_update) != 0)
	{
		//printf("e-Paper init failed\r\n");
	}

//	UBYTE *BlackImage;
//		/* you have to edit the startup_stm32fxxx.s file and set a big enough heap size */
//	UWORD Imagesize = ((EPD_WIDTH % 8 == 0)? (EPD_WIDTH / 8 ): (EPD_WIDTH / 8 + 1)) * EPD_HEIGHT;
//	if((BlackImage = (UBYTE *)malloc(Imagesize)) == NULL)
//	{
//		//printf("Failed to apply for black memory...\r\n");
//		//		return 0;
//	}
//	printf("Paint_NewImage\r\n");
}

void EpaperScreen_Clear()
{
	EPD_Clear();
}

void EpaperScreen_ForceSleepMode()
{
	EPD_Sleep();
}

void EpaperScreen_DisplayDefaultImage()
{
	EPD_Display(gImage_1in54);
}

void EpaperScreen_Display(const uint8_t imageBuffer[5000])
{
	//Paint_NewImage(BlackImage, EPD_WIDTH, EPD_HEIGHT, 270, WHITE);
	//Paint_SelectImage(BlackImage);
	//Paint_Clear(WHITE);
	//Paint_DrawBitMap(ImageBuffer);
	//Paint_DrawString_EN(70, 80, "Test2", &Font24, WHITE, BLACK);

//	EPD_Clear();
//	DEV_Delay_ms(500);
//	EPD_Clear();
//	DEV_Delay_ms(500);
	EPD_Display(imageBuffer);
//	DEV_Delay_ms(2000);
}
