
#ifndef LOGGING_HPP_INCLUDED
#define LOGGING_HPP_INCLUDED

#include "stm32l4xx_hal.h"

//#define ENABLE_UART_LOGGING

#ifdef ENABLE_UART_LOGGING
void Logging_Init(UART_HandleTypeDef* uartPeripheralHandle, uint32_t uartBaudRate);
#define Logging_Init(a, b)
#else
#endif

void Logging_Log(const char* logMsg, uint32_t logMsgLen);

void Logging_Log(const char* logMsg);


#endif // LOGGING_HPP_INCLUDED
