
#include "Logging.hpp"
#include "stm32l4xx_hal.h"
#include "stm32l4xx_hal_def.h"

#ifdef ENABLE_UART_LOGGING
static UART_HandleTypeDef* g_UARTPeripheralHandle = nullptr;
static uint32_t g_UARTBaudRate = 0;
#endif

static uint32_t Logging_StrLen(const char* str)
{
	const char* startPointer = str;

	if (str != nullptr)
	{
		while (*str != '\0')
		{
			str++;
		}
	}

	return (str - startPointer);
}

#ifdef ENABLE_UART_LOGGING
void Logging_Init(UART_HandleTypeDef* uartPeripheralHandle, uint32_t uartBaudRate)
{
	g_UARTPeripheralHandle = uartPeripheralHandle;
	g_UARTBaudRate = uartBaudRate;
}
#endif

void Logging_Log(const char* logMsg, uint32_t logMsgLen)
{
#ifdef ENABLE_UART_LOGGING
	if (g_UARTPeripheralHandle != nullptr)
	{
		const uint32_t numberOfUARTStopBits = logMsgLen;
		const uint32_t numberOfUARTDataBits = (logMsgLen * 8u);
		const uint32_t calculationErrorsCorrectionFactorInMs = 10;
		const uint32_t timeoutValueInMs = ((numberOfUARTDataBits + numberOfUARTStopBits) / (g_UARTBaudRate * 1000u));
		HAL_UART_Transmit(g_UARTPeripheralHandle, reinterpret_cast<uint8_t*>(const_cast<char*>(logMsg)), logMsgLen, (timeoutValueInMs + calculationErrorsCorrectionFactorInMs));
	}
#else
	(void)logMsg;
	(void)logMsgLen;
#endif
}

void Logging_Log(const char* logMsg)
{
	Logging_Log(logMsg, Logging_StrLen(logMsg));
}
