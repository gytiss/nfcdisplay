
#ifndef EEPROM_H_INCLUDED
#define EEPROM_H_INCLUDED

#include "stm32l4xx_hal.h"

void EEPROM_Init(I2C_HandleTypeDef* i2cDevice);

bool EEPROM_EnableGPO();

bool EEPROM_DisableEnergyHarvesting();

bool EEPROM_EnergyHarvestingEnabled();

bool EEPROM_ReadImage(uint8_t outImageData[5012]);

bool EEPROM_ReadMailBox(uint8_t* outBuffer, uint16_t numBytesToRead);

bool EEPROM_WriteMailBox(const uint8_t* buffer, uint16_t numBytesToWrite);

#endif // EEPROM_H_INCLUDED
