
#include "SuperCap.hpp"
#include "stm32l4xx_hal.h"
#include "stm32l4xx_it.h"

SuperCap::SuperCap(ADC_HandleTypeDef& adcHandle)
: m_ADCHandle(adcHandle),
  m_State(State::Idle),
  m_LatestVoltageValue(0u)
{
	Register_InterruptsHandler(InterruptID_ADC1, _adcInterruptHandler, this);
}

uint32_t SuperCap::getCurrentVoltage()
{
	m_State = State::WaitingForReading;
	HAL_ADC_Start_IT(&m_ADCHandle);

//	const uint32_t ADCSamplesToAverage = 1u;
//	uint32_t adcSampleValues = 0u;
//	uint32_t adcSamplesAcquired = 0u;
//
//	for(uint32_t i = 0; i < ADCSamplesToAverage; i++)
//	{
//		if (HAL_ADC_Start(&m_ADCHandle) == HAL_OK)
//		{
//			//ADCInitSuccess__ = true;
//
//			if (HAL_ADC_PollForConversion(&m_ADCHandle, 2000) == HAL_OK)
//			{
//				adcSamplesAcquired++;
//				adcSampleValues += HAL_ADC_GetValue(&m_ADCHandle);
//			}
//			HAL_ADC_Stop(&m_ADCHandle);
//		}
//	}
//
//	return (adcSampleValues / adcSamplesAcquired);

	while(m_State != State::ValueAvailable)
	{
	}

	return m_LatestVoltageValue;
}

void SuperCap::_adcInterruptHandler(void* thisPtr)
{
	SuperCap* superCapPtr = static_cast<SuperCap*>(thisPtr);

	superCapPtr->m_LatestVoltageValue = HAL_ADC_GetValue(&(superCapPtr->m_ADCHandle));
	superCapPtr->m_State = State::ValueAvailable;
}
