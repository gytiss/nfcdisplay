
#ifndef SUPERCAP_H_INCLUDED
#define SUPERCAP_H_INCLUDED

#include <cstdint>

struct __ADC_HandleTypeDef;
typedef __ADC_HandleTypeDef ADC_HandleTypeDef;

class SuperCap
{
public:
	SuperCap(ADC_HandleTypeDef& adcHandle);

	uint32_t getCurrentVoltage();

private:
	enum class State
	{
		Idle,
		WaitingForReading,
		ValueAvailable
	};

	static void _adcInterruptHandler(void* thisPtr);

	ADC_HandleTypeDef& m_ADCHandle;
	State m_State;
	uint32_t m_LatestVoltageValue;
};



#endif // SUPERCAP_H_INCLUDED
