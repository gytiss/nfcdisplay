#include "EEPROM.hpp"

#include "ST25DV/st25dv.h"
#include "ST25DV/st25dv_stm32_io.h"

#include "Logging.hpp"

void EEPROM_Init(I2C_HandleTypeDef* i2cDevice)
{
	ST25DV_STM32_IO_Init(i2cDevice);
	St25Dv_i2c_Drv.Init();
}

/*
#define ST25DV_GPO_DYN_RFUSERSTATE_SHIFT     (0)
#define ST25DV_GPO_DYN_RFUSERSTATE_FIELD     0xFE
#define ST25DV_GPO_DYN_RFUSERSTATE_MASK      0x01
#define ST25DV_GPO_DYN_RFACTIVITY_SHIFT      (1)
#define ST25DV_GPO_DYN_RFACTIVITY_FIELD      0xFD
#define ST25DV_GPO_DYN_RFACTIVITY_MASK       0x02
#define ST25DV_GPO_DYN_RFINTERRUPT_SHIFT     (2)
#define ST25DV_GPO_DYN_RFINTERRUPT_FIELD     0xFB
#define ST25DV_GPO_DYN_RFINTERRUPT_MASK      0x04
#define ST25DV_GPO_DYN_FIELDCHANGE_SHIFT     (3)
#define ST25DV_GPO_DYN_FIELDCHANGE_FIELD     0xF7
#define ST25DV_GPO_DYN_FIELDCHANGE_MASK      0x08
#define ST25DV_GPO_DYN_RFPUTMSG_SHIFT        (4)
#define ST25DV_GPO_DYN_RFPUTMSG_FIELD        0xEF
#define ST25DV_GPO_DYN_RFPUTMSG_MASK         0x10
#define ST25DV_GPO_DYN_RFGETMSG_SHIFT        (5)
#define ST25DV_GPO_DYN_RFGETMSG_FIELD        0xDF
#define ST25DV_GPO_DYN_RFGETMSG_MASK         0x20
#define ST25DV_GPO_DYN_RFWRITE_SHIFT         (6)
#define ST25DV_GPO_DYN_RFWRITE_FIELD         0xBF
#define ST25DV_GPO_DYN_RFWRITE_MASK          0x40
#define ST25DV_GPO_DYN_ENABLE_SHIFT          (7)
#define ST25DV_GPO_DYN_ENABLE_FIELD          0x7F
#define ST25DV_GPO_DYN_ENABLE_MASK           0x80
#define ST25DV_GPO_DYN_ALL_MASK              0xFF
 */

bool EEPROM_EnableGPO()
{
	volatile NFCTAG_StatusTypeDef retVal;

	uint8_t currentGPOValue = 0x00u;
	retVal = St25Dv_i2c_ExtDrv.ReadGPO_Dyn(&currentGPOValue);

	retVal = St25Dv_i2c_ExtDrv.ResetGPO_en_Dyn();

	retVal = St25Dv_i2c_ExtDrv.ReadGPO_Dyn(&currentGPOValue);

	ST25DV_I2CSSO_STATUS securitySessionStatus = ST25DV_SESSION_CLOSED;
	retVal = St25Dv_i2c_ExtDrv.ReadI2CSecuritySession_Dyn(&securitySessionStatus);
	if (retVal != NFCTAG_OK)
	{
		Logging_Log("Failed to read security status of the EEPROM");
	}
	else
	{
		if (securitySessionStatus == ST25DV_SESSION_CLOSED)
		{
			ST25DV_PASSWD eepromPassword;
			eepromPassword.MsbPasswd = 0x00000000u;
			eepromPassword.LsbPasswd = 0x00000000u;
			retVal = St25Dv_i2c_ExtDrv.PresentI2CPassword(eepromPassword);
			if (retVal != NFCTAG_OK)
			{
				Logging_Log("Failed to present password to the EEPROM");
			}

			if (retVal == NFCTAG_OK)
			{
				retVal = St25Dv_i2c_ExtDrv.ReadI2CSecuritySession_Dyn(&securitySessionStatus);
				if (retVal != NFCTAG_OK)
				{
					Logging_Log("Failed to read security status of the EEPROM after setting password");
				}
			}
		}
	}

	if (securitySessionStatus == ST25DV_SESSION_OPEN)
	{
		currentGPOValue =
		(
				ST25DV_GPO_DYN_RFINTERRUPT_MASK |
				ST25DV_GPO_DYN_ENABLE_MASK
		);
		retVal = St25Dv_i2c_Drv.ConfigIT(currentGPOValue);
		retVal = St25Dv_i2c_ExtDrv.ReadGPO_Dyn(&currentGPOValue);
	}

	return (retVal == NFCTAG_OK);
}

bool EEPROM_DisableEnergyHarvesting()
{
	return (St25Dv_i2c_ExtDrv.ResetEHENMode_Dyn() == NFCTAG_OK);
}

bool EEPROM_EnergyHarvestingEnabled()
{
	bool enabled = false;

	if (St25Dv_i2c_Drv.IsReady(1) == NFCTAG_OK)
	{
		ST25DV_EH_CTRL energyHarvestingStatus;
		volatile NFCTAG_StatusTypeDef retVal = St25Dv_i2c_ExtDrv.ReadEHCtrl_Dyn(&energyHarvestingStatus);

		if ((energyHarvestingStatus.EH_EN_Mode == ST25DV_ENABLE) && (energyHarvestingStatus.EH_on == ST25DV_ENABLE))
		{
			enabled = true;
		}
	}

	return enabled;
}

bool EEPROM_ReadImage(uint8_t outImageData[5012])
{
	volatile NFCTAG_StatusTypeDef retVal = St25Dv_i2c_Drv.ReadData(outImageData,  0x0000 , 5012);
	return (retVal == NFCTAG_OK);
}

bool EEPROM_ReadMailBox(uint8_t* outBuffer, uint16_t numBytesToRead)
{
	bool retVal = false;

	ST25DV_EN_STATUS mailBoxStatus = ST25DV_DISABLE;
	if (St25Dv_i2c_ExtDrv.ReadMBMode(&mailBoxStatus) == NFCTAG_OK)
	{
		if (mailBoxStatus == ST25DV_ENABLE)
		{
			uint16_t offset = 0U;
			retVal = (St25Dv_i2c_ExtDrv.ReadMailboxData(outBuffer, offset, numBytesToRead) == NFCTAG_OK);
		}
	}

	return retVal;
}

bool EEPROM_WriteMailBox(const uint8_t* buffer, uint16_t numBytesToWrite)
{
	bool retVal = false;

	ST25DV_EN_STATUS mailBoxStatus = ST25DV_DISABLE;
	if (St25Dv_i2c_ExtDrv.ReadMBMode(&mailBoxStatus) == NFCTAG_OK)
	{
		if (mailBoxStatus == ST25DV_ENABLE)
		{
			retVal = (St25Dv_i2c_ExtDrv.WriteMailboxData(buffer, numBytesToWrite) == NFCTAG_OK);
		}
	}

	return retVal;
}














