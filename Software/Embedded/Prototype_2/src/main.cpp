
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2019 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32l4xx_hal.h"
#include "stm32l4xx_hal_def.h"
#include "EpaperScreen.h"
#include "EEPROM.hpp"
#include "Logging.hpp"
#include "StringUtils.hpp"
#include "SuperCap/SuperCap.hpp"
#include "MillisecondTimer/MillisecondTimer.hpp"
#include "InbuiltImages/Image1_Compressed_w10_l3.h"
#include "InbuiltImages/Image2_Compressed_w10_l3.h"
#include "InbuiltImages/Image3_Compressed_w10_l3.h"
#include "Heatshrink/heatshrink_decoder.h"
#include <cstring>

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;

I2C_HandleTypeDef hi2c1;

RTC_HandleTypeDef hrtc;

SPI_HandleTypeDef hspi1;

TIM_HandleTypeDef htim2;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Deinit(void);
static void MX_GPIO_Init(void);
static void MX_I2C1_Init(void);
static void MX_ADC1_Init(void);
static void MX_SPI1_Init(void);
static void MX_RTC_Init(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

constexpr uint32_t ImageSizeInBytes = 5000U;
constexpr uint32_t ImageHeaderSizeInBytes = 12U;
static uint8_t g_ImageDataBuffer[ImageSizeInBytes + ImageHeaderSizeInBytes] = { 0x00 };
static uint8_t g_ImageDataDecompressionBuffer[sizeof(g_ImageDataBuffer)] = { 0x00 };
volatile HAL_StatusTypeDef uartRetVal;
static char strTempBuffer[51];

static bool PrintReadImage(const uint8_t* dataBytes, uint32_t dataBytesCount)
{
	bool retVal = true;
	const uint32_t lengthOfALineInBytes = 10;
	const uint32_t numberOfFullLines = (dataBytesCount / lengthOfALineInBytes);
	const uint32_t leftoverBytes = (dataBytesCount - (numberOfFullLines * lengthOfALineInBytes));

	const uint8_t* dataBuffer = dataBytes;

	for (uint32_t i = 0; retVal && (i < numberOfFullLines); i++)
	{
		uint32_t resultStrLen = 0;
		retVal = StringUtils::ByteArrayToHexString(dataBuffer, lengthOfALineInBytes, strTempBuffer, (sizeof(strTempBuffer) - 1), &resultStrLen);
		if (retVal)
		{
			strTempBuffer[resultStrLen] = '\n';
			Logging_Log(strTempBuffer, (resultStrLen + 1));
		}
		dataBuffer += lengthOfALineInBytes;
	}

	if (retVal && (leftoverBytes > 0))
	{
		uint32_t resultStrLen = 0;
		retVal = StringUtils::ByteArrayToHexString(dataBuffer, leftoverBytes, strTempBuffer, (sizeof(strTempBuffer) - 1), &resultStrLen);
		if (retVal)
		{
			strTempBuffer[resultStrLen] = '\n';
			Logging_Log(strTempBuffer, (resultStrLen + 1));
		}
	}

	return retVal;
}

static void SleepForNMilliseconds(uint32_t milliseconds)
{
	const uint32_t RTCCFreqInHz = 32000;
	const uint32_t FreqDivider = 16;

	MX_GPIO_Deinit();
	HAL_RTCEx_SetWakeUpTimer_IT(&hrtc, ((RTCCFreqInHz / FreqDivider) / 1000)  * milliseconds, RTC_WAKEUPCLOCK_RTCCLK_DIV16);
	HAL_SuspendTick();
	__HAL_FLASH_SLEEP_POWERDOWN_ENABLE();
	HAL_PWR_EnterSTOPMode(PWR_LOWPOWERREGULATOR_ON, PWR_SLEEPENTRY_WFE);
	SystemClock_Config();
	HAL_ResumeTick();
	MX_GPIO_Init();
}

static void ToggleCSPinNTimes(uint32_t times)
{
	const uint32_t milliseconds = 100;

	GPIO_PinState currentState = GPIO_PIN_SET;
	HAL_GPIO_WritePin(GPIOA, SPI1_CS_Pin, currentState);

	for (uint32_t i = 0; i < times; i++)
	{
		const uint32_t RTCCFreqInHz = 32000;
		const uint32_t FreqDivider = 16;
		HAL_RTCEx_SetWakeUpTimer_IT(&hrtc, ((RTCCFreqInHz / FreqDivider) / 1000)  * milliseconds, RTC_WAKEUPCLOCK_RTCCLK_DIV16);
		HAL_SuspendTick();
		__HAL_FLASH_SLEEP_POWERDOWN_ENABLE();
		HAL_PWR_EnterSTOPMode(PWR_LOWPOWERREGULATOR_ON, PWR_SLEEPENTRY_WFE);
		SystemClock_Config();
		HAL_ResumeTick();

		currentState = (GPIO_PinState)!currentState;
		HAL_GPIO_WritePin(GPIOA, SPI1_CS_Pin, currentState);
	}

	HAL_GPIO_WritePin(GPIOA, SPI1_CS_Pin, GPIO_PIN_SET);
}

constexpr uint32_t ADCVoltage2V = 2460;   // (Max_ADC_Range / Suppl_Voltage) * Desired_Voltage = ((4096.0 / 3.33) 2.0)
constexpr uint32_t ADCVoltage2_8V = 3444; // (Max_ADC_Range / Suppl_Voltage) * Desired_Voltage = ((4096.0 / 3.33) 2.8)
constexpr uint32_t ADCVoltage2_7V = 3319;
constexpr uint32_t ADCVoltage2_5V = 2900;
constexpr uint32_t ADCVoltage2_4V = 2765;

static void WaitTillVoltageIsAtLeastNVolts(SuperCap& sp, uint32_t voltageInADCUnits)
{
	uint32_t currentVoltage = 0u;
	SleepForNMilliseconds(500);
	do
	{
		currentVoltage = sp.getCurrentVoltage();
		if (currentVoltage < voltageInADCUnits)
		{
			SleepForNMilliseconds(1000);
		}
	}
	while (currentVoltage < voltageInADCUnits);
}

enum ImageTypeID
{
	ImageTypeID_InbuiltImage_FirstID = 0x01,
	ImageTypeID_InbuiltImage_1 = ImageTypeID_InbuiltImage_FirstID,
	ImageTypeID_InbuiltImage_2 = 0x02,
	ImageTypeID_InbuiltImage_3 = 0x03,

	ImageTypeID_InbuiltImage_InvalidID,
	ImageTypeID_InbuiltImage_Count = (ImageTypeID_InbuiltImage_InvalidID - 1U),

	ImageTypeID_CustomImage = 0x7E,
};

enum ImageDataType
{
	ImageDataType_Compressed = 0xCC,
	ImageDataType_Uncompressed = 0x00
};

enum ImageUpdateErrorCode
{
	ImageUpdateErrorCode_Success = 0x00,
	ImageUpdateErrorCode_FailedToReadEEPROM = 0x01,
	ImageUpdateErrorCode_UnknownImageTypeID = 0x02
};

struct InbuiltImage
{
	const uint8_t* data;
	const uint32_t dataSizeInBytes;
};

static const InbuiltImage InbuiltImages[ImageTypeID_InbuiltImage_Count]
{
	{ Image1_Compressed_w10_l3_bin, Image1_Compressed_w10_l3_bin_len },
	{ Image2_Compressed_w10_l3_bin, Image2_Compressed_w10_l3_bin_len },
	{ Image3_Compressed_w10_l3_bin, Image3_Compressed_w10_l3_bin_len },
};

static heatshrink_decoder g_HeatshrinkDecoder;

static bool DecompressImage(const uint8_t* compressedImage, uint32_t compressedImageSizeInBytes, uint8_t* outDecompressedImage, uint32_t decompressionBufferSize)
{
	bool retVal = true;

	heatshrink_decoder_reset(&g_HeatshrinkDecoder);

	while(retVal && (compressedImageSizeInBytes > 0))
	{
		size_t bytesConsumed = 0U;
		if (heatshrink_decoder_sink(&g_HeatshrinkDecoder, compressedImage, compressedImageSizeInBytes, &bytesConsumed) == HSDR_SINK_OK)
		{
			if (compressedImageSizeInBytes >= bytesConsumed)
			{
				compressedImage += bytesConsumed;
				compressedImageSizeInBytes -= bytesConsumed;
			}
			else
			{
				compressedImageSizeInBytes = 0U;
			}

			HSD_poll_res decodeResult = HSDR_POLL_EMPTY;
			HSD_finish_res finishResult = HSDR_FINISH_DONE;
			do
			{
				decodeResult = HSDR_POLL_EMPTY;

				if (compressedImageSizeInBytes == 0U)
				{
					finishResult = heatshrink_decoder_finish(&g_HeatshrinkDecoder);
				}

				if ((finishResult == HSDR_FINISH_MORE) || (compressedImageSizeInBytes > 0))
				{
					size_t bytesDecoded = 0U;
					decodeResult = heatshrink_decoder_poll(&g_HeatshrinkDecoder, outDecompressedImage, decompressionBufferSize, &bytesDecoded);
					if (decompressionBufferSize >= bytesDecoded)
					{
						outDecompressedImage += bytesDecoded;
						decompressionBufferSize -= bytesDecoded;
					}
					else
					{
						decompressionBufferSize = 0U;
					}
				}
			} while ((decodeResult == HSDR_POLL_MORE) || ((decodeResult == HSDR_POLL_EMPTY) && (finishResult == HSDR_FINISH_MORE)));

			if ((decodeResult != HSDR_POLL_EMPTY) || ((compressedImageSizeInBytes == 0U) && (finishResult != HSDR_FINISH_DONE)))
			{
				retVal = false;
			}
		}
		else
		{
			retVal = false;
		}
	}

	return retVal;
}

static const uint8_t* GetImageDataPointer(const uint8_t* imageData, const uint32_t imageDataSizeInBytes)
{
	const uint8_t* retVal = nullptr;

	static_assert(sizeof(g_ImageDataDecompressionBuffer) == sizeof(g_ImageDataBuffer), "Image buffers must be of equal size");
	static_assert(sizeof(g_ImageDataDecompressionBuffer) > ImageHeaderSizeInBytes, "Image decompression buffer size must be larger than image header size");

	if ((imageData != nullptr) && (imageDataSizeInBytes > ImageHeaderSizeInBytes))
	{
		constexpr uint32_t DataTypeFieldStartIndex = 0U;
		constexpr uint32_t DataLenFieldStartIndex = 1U;

		if (imageData[DataTypeFieldStartIndex] == ImageDataType_Compressed)
		{
			const uint32_t compressedImageDataSizeInBytes = ((((uint32_t)imageData[DataLenFieldStartIndex + 3U]) << 24U)|
															 (((uint32_t)imageData[DataLenFieldStartIndex + 2U]) << 16U)|
															 (((uint32_t)imageData[DataLenFieldStartIndex + 1U]) << 8U)|
															 imageData[DataLenFieldStartIndex]);

			const uint32_t maximumCompressedImageSizeInBytes = (imageDataSizeInBytes - ImageHeaderSizeInBytes);
			if (compressedImageDataSizeInBytes <= maximumCompressedImageSizeInBytes)
			{
				if (DecompressImage((imageData + ImageHeaderSizeInBytes), compressedImageDataSizeInBytes, (g_ImageDataDecompressionBuffer + ImageHeaderSizeInBytes), (sizeof(g_ImageDataDecompressionBuffer) - ImageHeaderSizeInBytes)))
				{
					retVal = g_ImageDataDecompressionBuffer;
				}
			}
		}
		else if (imageData[DataTypeFieldStartIndex] == ImageDataType_Uncompressed)
		{
			retVal = imageData;
		}
		else
		{
			// Unknown image type
			retVal = nullptr;
		}
	}

	return retVal;
}

template<size_t SIZE, class T> inline size_t STATIC_ARRAY(T (&arr)[SIZE]) {
    return SIZE;
}

static void PerformScreenUpdateSequence(SuperCap& sp, uint8_t receivedMailBoxCommand[4], const ImageTypeID imageTypeID)
{
	//MillisecondTimer timer(htim2);

	//	1) Super cap voltage above or equal to the expected voltage
	WaitTillVoltageIsAtLeastNVolts(sp, ADCVoltage2_4V);

	bool imageAvailable = false;
	ImageUpdateErrorCode imageUpdateErrorCode = ImageUpdateErrorCode_Success;
	const uint8_t* imageData = g_ImageDataBuffer;

	if (imageTypeID == ImageTypeID_CustomImage)
	{
		//	2) Read image from the EEPROM
		if (EEPROM_ReadImage(g_ImageDataBuffer))
		{
			imageData = GetImageDataPointer(g_ImageDataBuffer, sizeof(g_ImageDataBuffer));
			imageAvailable = (imageData != nullptr);
		}
		else
		{
			imageUpdateErrorCode = ImageUpdateErrorCode_FailedToReadEEPROM;
		}
	}
	else if ((imageTypeID >= ImageTypeID_InbuiltImage_FirstID) && (imageTypeID <= ImageTypeID_InbuiltImage_Count))
	{
		//	2) Get an inbuilt image
		if (imageTypeID > 0)
		{
			const uint32_t inbuiltImageIndex = (imageTypeID - 1U);
			if (inbuiltImageIndex < STATIC_ARRAY(InbuiltImages))
			{
				imageData = GetImageDataPointer(InbuiltImages[inbuiltImageIndex].data, InbuiltImages[inbuiltImageIndex].dataSizeInBytes);
				imageAvailable = (imageData != nullptr);
			}
		}
	}
	else
	{
		imageUpdateErrorCode = ImageUpdateErrorCode_UnknownImageTypeID;
	}

	if (imageAvailable)
	{
		// I2C peripheral no longer needed
		HAL_I2C_DeInit(&hi2c1);

		//	3) Super cap voltage above or equal to the expected voltage
		WaitTillVoltageIsAtLeastNVolts(sp, ADCVoltage2_4V);

		//	4) Clear screen
		MX_SPI1_Init();
		EpaperScreen_Init();
		EpaperScreen_Clear();
		EpaperScreen_ForceSleepMode();
		HAL_SPI_DeInit(&hspi1);

		//	5) Super cap voltage above or equal to the expected voltage
		WaitTillVoltageIsAtLeastNVolts(sp, ADCVoltage2_4V);

		//	6) Clear screen second time
		MX_SPI1_Init();
		EpaperScreen_Init();
		EpaperScreen_Clear();
		EpaperScreen_ForceSleepMode();
		HAL_SPI_DeInit(&hspi1);

		//	7) Super cap voltage above or equal to the expected voltage
		WaitTillVoltageIsAtLeastNVolts(sp, ADCVoltage2_4V);

		//	8) Write image to the screen
		MX_SPI1_Init();
		EpaperScreen_Init();
	    //EpaperScreen_DisplayDefaultImage();  // TODO replace with the real image
		EpaperScreen_Display(imageData + ImageHeaderSizeInBytes);
	    EpaperScreen_ForceSleepMode();
		HAL_SPI_DeInit(&hspi1);

		WaitTillVoltageIsAtLeastNVolts(sp, ADCVoltage2_4V);
	}

	if (imageAvailable)
	{
		receivedMailBoxCommand[0] = ~receivedMailBoxCommand[0];
		receivedMailBoxCommand[1] = ~receivedMailBoxCommand[1];
		receivedMailBoxCommand[2] = ~receivedMailBoxCommand[2];
		receivedMailBoxCommand[3] = ~receivedMailBoxCommand[3];
	}
	else
	{
		receivedMailBoxCommand[0] = 0xDE;
		receivedMailBoxCommand[1] = 0xAD;
		receivedMailBoxCommand[2] = imageUpdateErrorCode;
		receivedMailBoxCommand[3] = imageUpdateErrorCode;
	}

	bool mailBoxResponseSent = false;
	MX_I2C1_Init(); // Reenable I2C before writing the mailbox
	while(!mailBoxResponseSent)
	{
		if (EEPROM_WriteMailBox(receivedMailBoxCommand, 4U))
		{
			mailBoxResponseSent = true;
		}

		if (mailBoxResponseSent == false)
		{
			SleepForNMilliseconds(100);
		}
	}

}

bool SetBrownoutResetLevelTo2800Millivolts()
{
	volatile bool retVal = false;

	FLASH_OBProgramInitTypeDef optionByteInit;
	memset(&optionByteInit, 0xFF, sizeof(optionByteInit));

	optionByteInit.WRPArea = 0xFFFFFFFF;
	HAL_FLASHEx_OBGetConfig(&optionByteInit);

	const uint32_t readUserConfig = optionByteInit.USERConfig;

	memset(&optionByteInit, 0xFF, sizeof(optionByteInit));

	const uint32_t desiredBORLevel = OB_BOR_LEVEL_4;

	// If the level is not already set
	if ((readUserConfig & FLASH_OPTR_BOR_LEV_Msk) != desiredBORLevel)
	{
		optionByteInit.OptionType = OPTIONBYTE_USER;
		optionByteInit.USERType = OB_USER_BOR_LEV;
		optionByteInit.USERConfig = desiredBORLevel;

		HAL_FLASH_Unlock();

		if (HAL_FLASH_OB_Unlock() == HAL_OK)
		{
			if (HAL_FLASHEx_OBProgram(&optionByteInit) == HAL_OK)
			{
				// WARNING: This function call causes  MCU reset
				retVal = (HAL_FLASH_OB_Launch() == HAL_OK);
			}
			HAL_FLASH_OB_Lock();
		}

		 HAL_FLASH_Lock();
	}
	else
	{
		retVal = true;
	}

	return retVal;
}

volatile uint32_t iiiiii = 1u;
volatile uint32_t superCapVoltages[5] = { 0u };

/**
  * @brief  The application entry point.
  *
  * @retval None
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();





  MX_RTC_Init();

  //SleepForNMilliseconds(3000);

  SetBrownoutResetLevelTo2800Millivolts();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_I2C1_Init();
  MX_ADC1_Init();
  MX_SPI1_Init();
  /* USER CODE BEGIN 2 */

  /* USER CODE END 2 */

  //Logging_Init(&huart1, 115200);



	HAL_NVIC_SetPriority(ADC1_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(ADC1_IRQn);
	HAL_NVIC_SetPriority(EXTI1_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(EXTI1_IRQn);

	//HAL_NVIC_SetPriority(TIM2_IRQn, 0, 0);
	//HAL_NVIC_EnableIRQ(TIM2_IRQn);

	SuperCap sp(hadc1);

	WaitTillVoltageIsAtLeastNVolts(sp, ADCVoltage2_4V);

	// Initialise EEPROM
	MX_I2C1_Init();
	EEPROM_Init(&hi2c1);

	while(true)
	{
		const uint8_t HeaderByte0Index = 0;
		const uint8_t HeaderByte1Index = 1;
		const uint8_t HeaderByte2Index = 2;
		const uint8_t ImageTypeIndex = 3;

		bool updateScreenCommandReceived = false;
		uint8_t mailBoxData[4] = { 0U };
		while(!updateScreenCommandReceived)
		{
			if (EEPROM_ReadMailBox(mailBoxData, sizeof(mailBoxData)))
			{
				if ((mailBoxData[HeaderByte0Index] == 0xCA) &&
					(mailBoxData[HeaderByte1Index] == 0xFE) &&
					(mailBoxData[HeaderByte2Index] == 0xAF) &&
					((mailBoxData[ImageTypeIndex] == ImageTypeID_CustomImage) || ((ImageTypeID_InbuiltImage_Count > 0U) && ((mailBoxData[ImageTypeIndex] >= ImageTypeID_InbuiltImage_FirstID) && (mailBoxData[ImageTypeIndex] <= ImageTypeID_InbuiltImage_Count)))))
				{
					updateScreenCommandReceived = true;
				}
			}

			if (updateScreenCommandReceived == false)
			{
				SleepForNMilliseconds(1000);
			}
		}

		PerformScreenUpdateSequence(sp, mailBoxData, static_cast<ImageTypeID>(mailBoxData[ImageTypeIndex]));
	}


	// TODO remove this later, this is used to make sure that we can attach to the device with debugger on startup, because later the device will be in sleep mode
	for (volatile uint32_t  aaa = 0; aaa < 10000000; aaa++)
	{

	}

	while (1)
	{
		SleepForNMilliseconds(5000);
	}


	//
//	//__HAL_PWR_CLEAR_FLAG(PWR_FLAG_WU);
//	HAL_RTCEx_SetWakeUpTimer_IT(&hrtc, 2  * 9000, RTC_WAKEUPCLOCK_RTCCLK_DIV16);
//	HAL_SuspendTick();
//	__HAL_FLASH_SLEEP_POWERDOWN_ENABLE();
//	//HAL_PWR_EnterSLEEPMode(PWR_LOWPOWERREGULATOR_ON, PWR_SLEEPENTRY_WFE);
//	HAL_PWR_EnterSTOPMode(PWR_LOWPOWERREGULATOR_ON, PWR_SLEEPENTRY_WFE);
//	HAL_ResumeTick();

	//__HAL_RTC_WAKEUPTIMER_CLEAR_FLAG(&hrtc, RTC_CLEAR_WUTF);

//	HAL_RTCEx_SetWakeUpTimer_IT(&hrtc, 48000, RTC_WAKEUPCLOCK_RTCCLK_DIV2);
//	HAL_SuspendTick();
//	__HAL_FLASH_SLEEP_POWERDOWN_ENABLE();
//	HAL_PWR_EnterSLEEPMode(PWR_LOWPOWERREGULATOR_ON, PWR_SLEEPENTRY_WFE);
//	HAL_ResumeTick();

	while (1)
	{

	}

  //HAL_ADC_Start(&hadc1);
  //HAL_ADC_Start_IT(&hadc1);
//  iiiiii = 1;
//  while(iiiiii)
//  {
//  }

  //superCapVoltages[0] = sp.getCurrentVoltage();
  EpaperScreen_Init();
  //superCapVoltages[1] = sp.getCurrentVoltage();


  //timer.sleepForMs(300000);
 // timer.sleepForMs(3000);
 // timer.sleepForMs(3000);


  // Clear screen first time
  EpaperScreen_Clear();
  EpaperScreen_ForceSleepMode();
  //superCapVoltages[2] = sp.getCurrentVoltage(); // TODO go to sleep mode and wake up after a few milliseconds if voltage is too low
  EpaperScreen_Init();

  // CLear screen second time
  EpaperScreen_Clear();
  EpaperScreen_ForceSleepMode();
  //superCapVoltages[3] = sp.getCurrentVoltage(); // TODO go to sleep mode and wake up after a few milliseconds if voltage is too low
  EpaperScreen_Init();

  // Display the image on the screen
  EpaperScreen_DisplayDefaultImage();
  EpaperScreen_ForceSleepMode();
  //superCapVoltages[4] = sp.getCurrentVoltage(); // TODO go to sleep mode and wake up after a few milliseconds if voltage is too low

  EEPROM_Init(&hi2c1);

  EEPROM_EnableGPO();

  EEPROM_DisableEnergyHarvesting();

  if (EEPROM_ReadImage(g_ImageDataBuffer))
  {
	  while(1)
	  {
	  }
  }


  /* Infinite loop */
  /* USER CODE BEGIN WHILE */

  Logging_Log("Waiting for Energy Harvesting to be activated\n");

  while (1)
  {
	  if (EEPROM_EnergyHarvestingEnabled())
	  {
		  Logging_Log("Energy Harvesting activation detected\n");
		  Logging_Log("Reading the EEPROM\n");
		  if (EEPROM_ReadImage(g_ImageDataBuffer))
		  {
			  Logging_Log("Image data read from the EEPROM:\n");
			  PrintReadImage(g_ImageDataBuffer, sizeof(g_ImageDataBuffer));

			  const uint32_t headerBytes = 10;
			  EpaperScreen_Display(g_ImageDataBuffer + headerBytes);
			  EEPROM_DisableEnergyHarvesting();
		  }
		  else
		  {
			  Logging_Log("Failed to read image data from the EEPROM\n");
		  }
	  }

	  HAL_Delay(250);


  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */

  }
  /* USER CODE END 3 */

}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_LSI
                              |RCC_OSCILLATORTYPE_MSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.MSIState = RCC_MSI_ON;
  RCC_OscInitStruct.MSICalibrationValue = 0;
  RCC_OscInitStruct.MSIClockRange = RCC_MSIRANGE_5;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_MSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_RTC|RCC_PERIPHCLK_I2C1
                              |RCC_PERIPHCLK_ADC;
  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_PCLK1;
  PeriphClkInit.AdcClockSelection = RCC_ADCCLKSOURCE_PLLSAI1;
  PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSI;
  PeriphClkInit.PLLSAI1.PLLSAI1Source = RCC_PLLSOURCE_HSI;
  PeriphClkInit.PLLSAI1.PLLSAI1M = 4;
  PeriphClkInit.PLLSAI1.PLLSAI1N = 16;
  PeriphClkInit.PLLSAI1.PLLSAI1P = RCC_PLLP_DIV7;
  PeriphClkInit.PLLSAI1.PLLSAI1Q = RCC_PLLQ_DIV2;
  PeriphClkInit.PLLSAI1.PLLSAI1R = RCC_PLLR_DIV8;
  PeriphClkInit.PLLSAI1.PLLSAI1ClockOut = RCC_PLLSAI1_ADC1CLK;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the main internal regulator output voltage 
    */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* ADC1 init function */
static void MX_ADC1_Init(void)
{

  ADC_ChannelConfTypeDef sConfig;

    /**Common config 
    */
  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_ASYNC_DIV1;
  hadc1.Init.Resolution = ADC_RESOLUTION_12B;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.ScanConvMode = ADC_SCAN_DISABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  hadc1.Init.LowPowerAutoWait = DISABLE;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.NbrOfConversion = 1;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc1.Init.DMAContinuousRequests = DISABLE;
  hadc1.Init.Overrun = ADC_OVR_DATA_PRESERVED;
  hadc1.Init.OversamplingMode = DISABLE;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure Regular Channel 
    */
  sConfig.Channel = ADC_CHANNEL_15;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = ADC_SAMPLETIME_2CYCLES_5;
  sConfig.SingleDiff = ADC_SINGLE_ENDED;
  sConfig.OffsetNumber = ADC_OFFSET_NONE;
  sConfig.Offset = 0;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* I2C1 init function */
static void MX_I2C1_Init(void)
{

  hi2c1.Instance = I2C1;
  hi2c1.Init.Timing = 0x00000509;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure Analogue filter 
    */
  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c1, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure Digital filter 
    */
  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c1, 0) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* RTC init function */
static void MX_RTC_Init(void)
{

  /* USER CODE BEGIN RTC_Init 0 */

  /* USER CODE END RTC_Init 0 */

  /* USER CODE BEGIN RTC_Init 1 */

  /* USER CODE END RTC_Init 1 */

    /**Initialize RTC Only 
    */
  hrtc.Instance = RTC;
  hrtc.Init.HourFormat = RTC_HOURFORMAT_24;
  hrtc.Init.AsynchPrediv = 127;
  hrtc.Init.SynchPrediv = 249;
  hrtc.Init.OutPut = RTC_OUTPUT_DISABLE;
  hrtc.Init.OutPutRemap = RTC_OUTPUT_REMAP_NONE;
  hrtc.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
  hrtc.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;
  if (HAL_RTC_Init(&hrtc) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* SPI1 init function */
static void MX_SPI1_Init(void)
{

  /* SPI1 parameter configuration*/
  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi1.Init.NSS = SPI_NSS_SOFT;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_4;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 7;
  hspi1.Init.CRCLength = SPI_CRC_LENGTH_DATASIZE;
  hspi1.Init.NSSPMode = SPI_NSS_PULSE_DISABLE;
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

static void MX_GPIO_Deinit(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;

	/* GPIO Ports Clock Disable */
	__HAL_RCC_GPIOC_CLK_DISABLE();
	__HAL_RCC_GPIOA_CLK_DISABLE();
	__HAL_RCC_GPIOB_CLK_DISABLE();
	__HAL_RCC_GPIOH_CLK_DISABLE();

	/*Configure GPIO pins : RST_Pin DC_Pin SPI1_CS_Pin */
	HAL_GPIO_DeInit(GPIOA, RST_Pin|DC_Pin|SPI1_CS_Pin);

	/*Configure GPIO pin : BUSY_Pin */
	HAL_GPIO_DeInit(BUSY_GPIO_Port, BUSY_Pin);

	/*Configure GPIO pin : NFC_GPO_Pin */
	HAL_GPIO_DeInit(NFC_GPO_GPIO_Port, NFC_GPO_Pin);

	/*Configure GPIO pin : PH3 */
	HAL_GPIO_DeInit(GPIOH, GPIO_PIN_3);
}

/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
*/
static void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, RST_Pin|DC_Pin|SPI1_CS_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : PC14 PC15 */
  GPIO_InitStruct.Pin = GPIO_PIN_14|GPIO_PIN_15;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : PA0 PA1 PA9 PA10 
                           PA11 PA12 PA15 */
  GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_9|GPIO_PIN_10 
                          |GPIO_PIN_11|GPIO_PIN_12|GPIO_PIN_15;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : RST_Pin DC_Pin SPI1_CS_Pin */
  GPIO_InitStruct.Pin = RST_Pin|DC_Pin|SPI1_CS_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : BUSY_Pin */
  GPIO_InitStruct.Pin = BUSY_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(BUSY_GPIO_Port, &GPIO_InitStruct);

//  /*Configure GPIO pin : NFC_GPO_Pin */
//  GPIO_InitStruct.Pin = NFC_GPO_Pin;
//  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
//  GPIO_InitStruct.Pull = GPIO_NOPULL;
//  HAL_GPIO_Init(NFC_GPO_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : PB3 PB4 PB5 */
  GPIO_InitStruct.Pin = GPIO_PIN_3|GPIO_PIN_4|GPIO_PIN_5;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : PH3 */
  GPIO_InitStruct.Pin = GPIO_PIN_3;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOH, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  file: The file name as string.
  * @param  line: The line in file as a number.
  * @retval None
  */
void _Error_Handler(char *file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
