Boost Converter:
    (TPS61261DRVR)
    https://www.mouser.de/ProductDetail/Texas-Instruments/TPS61261DRVR?qs=sGAEpiMZZMtitjHzVIkrqXb0LWidhrTQyoPS5oUz9nE%3d
    
Boost Converter Inductor:
    (DFE252008C-4R7M=P2) x 1
    https://www.digikey.de/product-detail/en/murata-electronics-north-america/DFE252008C-4R7M=P2/490-10632-1-ND/5272060
    
Boost Converter Current Control Resistor:
    (RC0603FR-072K32L) x 1
    https://www.digikey.com/product-detail/en/yageo/RC0603FR-072K32L/311-2.32KHRCT-ND/729968
    
Linear Regulator:
    (MCP1711T-30I/5X)
    https://www.digikey.de/product-detail/en/microchip-technology/MCP1711T-30I-5X/MCP1711T-30I-5XCT-ND/5358302
    
Linear Regulator Current Limiting Resistor:
    (RCS040249R9FKED) x 2
    https://www.digikey.com/product-detail/en/vishay-dale/RCS040249R9FKED/541-2884-1-ND/5867177

MCU:
    (STM32L432KBU6)
    https://www.mouser.de/ProductDetail/STMicroelectronics/STM32L432KBU6?qs=sGAEpiMZZMuoKKEcg8mMKHeGRAWWvG585jKBSL%2fU%2fLLR0OrYDMDR3A%3d%3d
    
MCU Boot Mode Resistor:
    (RC0402FR-0710KL) x 1
    https://www.digikey.de/product-detail/en/yageo/RC0402FR-0710KL/311-10.0KLRCT-ND/729470

NFC EEPROM:
    (ST25DV64K-JFR6D3)
    https://www.mouser.de/ProductDetail/STMicroelectronics/ST25DV64K-JFR6D3?qs=%2fha2pyFadujvqhA3JlHiR5FRFD3zGqL0vssg%2fwZSxI3gD1g26FMp7vw4lPM7laZt&utm_source=octopart&utm_medium=aggregator&utm_campaign=511-ST25DV64K-JFR6D3&utm_content=STMicroelectronics

    or if above unavailable then:
    
    (ST25DV64K-JFR8D3)
    https://www.mouser.de/ProductDetail/STMicroelectronics/ST25DV64K-JFR8D3?qs=sGAEpiMZZMve4%2fbfQkoj%252bONsZkfEMdsaM6r7D%252bUKvGw%3d
    
Decoupling Capacitors 100nF:
    (CL05B104KO5NNNC):
    https://www.digikey.de/product-detail/en/samsung-electro-mechanics/CL05B104KO5NNNC/1276-1001-1-ND/3889087
    
Super Capacitor:
    https://www.digikey.com/product-detail/en/seiko-instruments/CPH3225A-2K/728-1067-1-ND/4747400
    
E-Paper Display Connector:
    (FH19C-24S-0.5SH(10)) x 1
    https://www.digikey.com/products/en?keywords=H125818DKR-ND
    https://www.hirose.com/product/en/products/FH19C__FH19SC/FH19C%2D24S%2D0.5SH%2810%29/
    
E-Paper Display Supporting Circuit:
    Inductor:
        (LQH2MPZ680MGRL) x 1
        https://www.digikey.com/product-detail/en/murata-electronics-north-america/LQH2MPZ680MGRL/490-15934-1-ND/6800599
    MOSFET:
        (FK3503010L) x 1
        https://www.digikey.com/product-detail/en/panasonic-electronic-components/FK3503010L/FK3503010LCT-ND/3719838
    DIODE:
        (RSX051VAM30TR) x 3
        https://www.digikey.com/product-detail/en/rohm-semiconductor/RSX051VAM30TR/RSX051VAM30CT-ND/7430509
    Capacitor 1uF/50V:
        (CL10A105KB8NNNC) x 9 + 4(to replace 4.7uF)
        https://www.digikey.com/product-detail/en/samsung-electro-mechanics/CL10A105KB8NNNC/1276-1860-1-ND/3889946
    Capacitor 10uF/6.3V:
        (JMK107BJ106MA-T) x 1
        https://www.digikey.nl/product-detail/en/JMK107BJ106MA-T/587-1256-1-ND/931033?utm_campaign=buynow&WT.z_cid=ref_octopart_dkc_buynow&utm_medium=aggregator&curr=eur&site=us&utm_source=octopart
    Resistor 1M:
        (RC0402FR-071ML) x 1
        https://www.digikey.com/product-detail/en/yageo/RC0402FR-071ML/311-1.00MLRCT-ND/729462
    Resistor 1K:
        (RC0402FR-071KL) x 1
        https://www.digikey.com/product-detail/en/yageo/RC0402FR-071KL/311-1.00KLRCT-ND/729460
    Resistor 3 Ohm:
        (RC0805FR-073RL) x 1
        https://www.digikey.com/product-detail/en/yageo/RC0805FR-073RL/311-3.00CRCT-ND/730746
        
